﻿using DataBaseFinalProject.Models;

namespace DataBaseFinalProject.ViewModels;

public class CreateExpenseTransactionViewModel
{
    public List<User> Users { get; set; }
    public List<ExpenseCategory> ExpenseCategories { get; set; }
    public ExpenseTransaction ExpenseTransaction { get; set; }
}