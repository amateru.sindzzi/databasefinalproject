﻿using DataBaseFinalProject.Models;

namespace DataBaseFinalProject.ViewModels;

public class CreateIncomeTransactionViewModel
{
    public List<User> Users { get; set; }
    public List<IncomeCategory> IncomeCategories { get; set; }
    public IncomeTransaction IncomeTransaction { get; set; }
}