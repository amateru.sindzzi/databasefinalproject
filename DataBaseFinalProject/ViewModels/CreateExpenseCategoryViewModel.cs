﻿using DataBaseFinalProject.Models;

namespace DataBaseFinalProject.ViewModels;

public class CreateExpenseCategoryViewModel
{
    public List<User> Users { get; set; }
    public ExpenseCategory ExpenseCategory { get; set; }
}