﻿using DataBaseFinalProject.Models;

namespace DataBaseFinalProject.ViewModels;

public class CreateIncomeCategoryViewModel
{
    public List<User> Users { get; set; }
    public IncomeCategory IncomeCategory { get; set; }
}