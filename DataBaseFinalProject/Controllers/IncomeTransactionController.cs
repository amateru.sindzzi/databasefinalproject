﻿using DataBaseFinalProject.Models;
using DataBaseFinalProject.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DataBaseFinalProject.Controllers;

public class IncomeTransactionController(DatabaseContext context) : Controller
{
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var incomeTransactions = await context.IncomeTransactions
            .Include(u => u.User)
            .Include(c => c.Category)
            .AsNoTracking()
            .ToListAsync();
        return View(incomeTransactions);
    }

    [HttpGet]
    public IActionResult Create()
    {
        var viewModel = new CreateIncomeTransactionViewModel
        {
            Users = context.Users.AsNoTracking().ToList(),
            IncomeCategories = context.IncomeCategories.AsNoTracking().ToList(),
            IncomeTransaction = new IncomeTransaction()
        };

        return View(viewModel);
    }

    [HttpPost]
    public IActionResult Create(CreateIncomeTransactionViewModel model)
    {
        model.IncomeTransaction.Id = Guid.NewGuid();
        context.IncomeTransactions.Add(model.IncomeTransaction);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
    
    [HttpPost]
    public IActionResult Delete(Guid id)
    {
        var incomeTransaction = context.IncomeTransactions.Find(id);
        if (incomeTransaction == null)
        {
            return NotFound();
        }

        context.IncomeTransactions.Remove(incomeTransaction);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
    
    [HttpGet]
    public IActionResult Edit(Guid id)
    {
        var incomeTransaction = context.IncomeTransactions.Find(id);
        if (incomeTransaction == null)
        {
            return NotFound();
        }

        var viewModel = new CreateIncomeTransactionViewModel
        {
            Users = context.Users.AsNoTracking().ToList(),
            IncomeCategories = context.IncomeCategories.AsNoTracking().ToList(),
            IncomeTransaction = context.IncomeTransactions.Find(id)!
        };

        return View(viewModel);
    }

    [HttpPost]
    public IActionResult Edit(CreateIncomeTransactionViewModel model)
    {
        context.IncomeTransactions.Update(model.IncomeTransaction);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
}