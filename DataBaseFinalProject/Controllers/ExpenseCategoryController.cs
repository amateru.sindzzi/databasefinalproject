﻿using DataBaseFinalProject.Models;
using DataBaseFinalProject.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DataBaseFinalProject.Controllers;

public class ExpenseCategoryController(DatabaseContext context) : Controller
{
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var expenseCategories = await context.ExpenseCategories.AsNoTracking().ToListAsync();
        return View(expenseCategories);
    }

    [HttpGet]
    public IActionResult Create()
    {
        var viewModel = new CreateExpenseCategoryViewModel
        {
            Users = context.Users.AsNoTracking().ToList(),
            ExpenseCategory = new ExpenseCategory()
        };

        return View(viewModel);
    }

    [HttpPost]
    public IActionResult Create(CreateExpenseCategoryViewModel model)
    {
        model.ExpenseCategory.Id = Guid.NewGuid();
        context.ExpenseCategories.Add(model.ExpenseCategory);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
    
    [HttpPost]
    public IActionResult Delete(Guid id)
    {
        var expenseCategory = context.ExpenseCategories.Find(id);
        if (expenseCategory == null)
        {
            return NotFound();
        }

        context.ExpenseCategories.Remove(expenseCategory);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
    
    [HttpGet]
    public IActionResult Edit(Guid id)
    {
        var expenseCategory = context.ExpenseCategories.Find(id);
        if (expenseCategory == null)
        {
            return NotFound();
        }

        var viewModel = new CreateExpenseCategoryViewModel
        {
            Users = context.Users.AsNoTracking().ToList(),
            ExpenseCategory = context.ExpenseCategories.Find(id)!
        };

        return View(viewModel);
    }

    [HttpPost]
    public IActionResult Edit(CreateExpenseCategoryViewModel model)
    {
        context.ExpenseCategories.Update(model.ExpenseCategory);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
}