﻿using DataBaseFinalProject.Models;
using DataBaseFinalProject.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DataBaseFinalProject.Controllers;

public class ExpenseTransactionController(DatabaseContext context) : Controller
{
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var expenseTransactions = await context.ExpenseTransactions
            .Include(u => u.User)
            .Include(c => c.Category)
            .AsNoTracking()
            .ToListAsync();
        return View(expenseTransactions);
    }

    [HttpGet]
    public IActionResult Create()
    {
        var viewModel = new CreateExpenseTransactionViewModel
        {
            Users = context.Users.AsNoTracking().ToList(),
            ExpenseCategories = context.ExpenseCategories.AsNoTracking().ToList(),
            ExpenseTransaction = new ExpenseTransaction()
        };

        return View(viewModel);
    }

    [HttpPost]
    public IActionResult Create(CreateExpenseTransactionViewModel model)
    {
        model.ExpenseTransaction.Id = Guid.NewGuid();
        context.ExpenseTransactions.Add(model.ExpenseTransaction);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
    
    [HttpPost]
    public IActionResult Delete(Guid id)
    {
        var expenseTransaction = context.ExpenseTransactions.Find(id);
        if (expenseTransaction == null)
        {
            return NotFound();
        }

        context.ExpenseTransactions.Remove(expenseTransaction);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
    
    [HttpGet]
    public IActionResult Edit(Guid id)
    {
        var expenseTransaction = context.ExpenseTransactions.Find(id);
        if (expenseTransaction == null)
        {
            return NotFound();
        }

        var viewModel = new CreateExpenseTransactionViewModel
        {
            Users = context.Users.AsNoTracking().ToList(),
            ExpenseCategories = context.ExpenseCategories.AsNoTracking().ToList(),
            ExpenseTransaction = context.ExpenseTransactions.Find(id)!
        };

        return View(viewModel);
    }

    [HttpPost]
    public IActionResult Edit(CreateExpenseTransactionViewModel model)
    {
        context.ExpenseTransactions.Update(model.ExpenseTransaction);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
}