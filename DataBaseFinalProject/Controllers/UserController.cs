﻿using DataBaseFinalProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DataBaseFinalProject.Controllers;

public class UserController(DatabaseContext context) : Controller
{
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var users = await context.Users.AsNoTracking().ToListAsync();
        return View(users);
    }

    [HttpGet]
    public IActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public IActionResult Create(User user)
    {
        if (ModelState.IsValid)
        {
            user.Id = Guid.NewGuid();
            context.Users.Add(user);
            context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }
        return RedirectToAction("Index");
    }
    
    [HttpPost]
    public IActionResult Delete(Guid id)
    {
        var client = context.Users.Find(id);
        if (client == null)
        {
            return NotFound();
        }

        context.Users.Remove(client);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
    
    [HttpGet]
    public IActionResult Edit(Guid clientId)
    {
        var client = context.Users.Find(clientId);
        if (client == null)
        {
            return NotFound();
        }
        return View(client);
    }

    [HttpPost]
    public IActionResult Edit(User user)
    {
        context.Users.Update(user);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
}