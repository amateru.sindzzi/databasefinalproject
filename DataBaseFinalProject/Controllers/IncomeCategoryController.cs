﻿using DataBaseFinalProject.Models;
using DataBaseFinalProject.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DataBaseFinalProject.Controllers;

public class IncomeCategoryController(DatabaseContext context) : Controller
{
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        var incomeCategories = await context.IncomeCategories.AsNoTracking().ToListAsync();
        return View(incomeCategories);
    }

    [HttpGet]
    public IActionResult Create()
    {
        var viewModel = new CreateIncomeCategoryViewModel
        {
            Users = context.Users.AsNoTracking().ToList(),
            IncomeCategory = new IncomeCategory()
        };

        return View(viewModel);
    }

    [HttpPost]
    public IActionResult Create(CreateIncomeCategoryViewModel model)
    {
        model.IncomeCategory.Id = Guid.NewGuid();
        context.IncomeCategories.Add(model.IncomeCategory);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
    
    [HttpPost]
    public IActionResult Delete(Guid id)
    {
        var incomeCategory = context.IncomeCategories.Find(id);
        if (incomeCategory == null)
        {
            return NotFound();
        }

        context.IncomeCategories.Remove(incomeCategory);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
    
    [HttpGet]
    public IActionResult Edit(Guid id)
    {
        var incomeCategory = context.IncomeCategories.Find(id);
        if (incomeCategory == null)
        {
            return NotFound();
        }

        var viewModel = new CreateIncomeCategoryViewModel
        {
            Users = context.Users.AsNoTracking().ToList(),
            IncomeCategory = context.IncomeCategories.Find(id)!
        };

        return View(viewModel);
    }

    [HttpPost]
    public IActionResult Edit(CreateIncomeCategoryViewModel model)
    {
        context.IncomeCategories.Update(model.IncomeCategory);
        context.SaveChanges();
        return RedirectToAction("Index");
    }
}