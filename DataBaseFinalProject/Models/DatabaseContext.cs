﻿using Microsoft.EntityFrameworkCore;

namespace DataBaseFinalProject.Models;

public class DatabaseContext(DbContextOptions<DatabaseContext> options)
    : DbContext(options)
{
    public DbSet<User> Users { get; set; }
    public DbSet<ExpenseCategory> ExpenseCategories { get; set; }
    public DbSet<IncomeCategory> IncomeCategories { get; set; }
    public DbSet<ExpenseTransaction> ExpenseTransactions { get; set; }
    public DbSet<IncomeTransaction> IncomeTransactions { get; set; }
}