﻿namespace DataBaseFinalProject.Models;

public class User
{
    public Guid Id { get; set; }
    public string Nickname { get; set; }
    public DateTime Birthdate { get; set; }
}