﻿namespace DataBaseFinalProject.Models;

public class ExpenseCategory
{
    public Guid Id { get; set; }
    public Guid UserId { get; set; }
    public User User { get; set; }
    public string Name { get; set; }
    public DateTime CreatedAt { get; set; }
}