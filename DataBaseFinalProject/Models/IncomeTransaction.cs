﻿namespace DataBaseFinalProject.Models;

public class IncomeTransaction
{
    public Guid Id { get; set; }
    public Guid UserId { get; set; }
    public User User { get; set; }
    public decimal Amount { get; set; }
    public string Description { get; set; }
    public Guid CategoryId { get; set; }
    public IncomeCategory Category { get; set; }
    public DateTime CreatedAt { get; set; }
}